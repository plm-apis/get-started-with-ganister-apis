# Get Started with Ganister PLM API
Welcome to this documentation where we will quickly help you connect to your Ganister PLM API.

## REST API Introduction
Ganister PLM follows an API-first development pattern. Every feature you may see in our client application is first developed in Ganister PLM server as a REST API Endpoint.

## API Training Flow
<div class="center">

```mermaid
graph TD
    A[Start API Training] --> B(Log In)
     subgraph Authentication
    B --> C{2FA Enabled?}
    C -->|Yes| D[SignIn with 2FA Code]
    end
    subgraph Data Creation
    D --> E
    C --> E[Create Part]
    E --> F[Create Document]
    F --> G[Attach Document to Part]
    end
    subgraph Data Fetching
    G --> H[Get All Parts]
    H --> I[Search for a Document]
    I --> J[Retrieve Part]
    J --> K[Retrieve Document attached to Part]
    end
    K --> L[Training Done !]
```

</div>

## Authentication

Ganister PLM API uses a token to continuously authenticate every exchange between a client and the server. 

### Login

#### Query
![Login](./images/login1.png)

#### Result
```json
{
    "_type": "user",
    "_id": "0700d950-654f-11eb-bcc0-c5cae311491a",
    "properties": {
        "lastName": "Istrator",
        "_createdOn": 1612267565925,
        "active": true,
        "language": "fr",
        "_createdBy": "0700d950-654f-11eb-bcc0-c5cae311491a",
        "_lockState": false,
        "_versionedOn": 1631710912475,
        "firstName": "Admin",
        "_lockedOn": 1631710912475,
        "_createdByName": "Admin",
        "_state": "draft",
        "_isAdmin": true,
        "_tracked": false,
        "_id": "0700d950-654f-11eb-bcc0-c5cae311491a",
        "_lockable": true,
        "pict": "images/userLogo.png",
        "email": "test@ganister.eu",
        "_modifiedOn": 1633617368844,
        "name": "Admin Istrator",
        "_labelRef": "Admin Istrator",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJHYW5pc3RlckFQSSIsInN1YiI6IjA3MDBkOTUwLTY1NGYtMTFlYi1iY2MwLWM1Y2FlMzExNDkxYSIsImlhdCI6MTYzMzk4NjQ0NDk1NCwiZXhwIjoxNjMzOTkzNjQ0OTU0LCJsYXN0TmFtZSI6IklzdHJhdG9yIiwiX2NyZWF0ZWRPbiI6MTYxMjI2NzU2NTkyNSwiYWN0aXZlIjp0cnVlLCJsYW5ndWFnZSI6ImZyIiwiX2NyZWF0ZWRCeSI6IjA3MDBkOTUwLTY1NGYtMTFlYi1iY2MwLWM1Y2FlMzExNDkxYSIsIl9sb2NrU3RhdGUiOmZhbHNlLCJfdmVyc2lvbmVkT24iOjE2MzE3MTA5MTI0NzUsImZpcnN0TmFtZSI6IkFkbWluIiwiX2xvY2tlZE9uIjoxNjMxNzEwOTEyNDc1LCJfY3JlYXRlZEJ5TmFtZSI6IkFkbWluIiwiX3N0YXRlIjoiZHJhZnQiLCJfaXNBZG1pbiI6dHJ1ZSwiX3RyYWNrZWQiOmZhbHNlLCJfaWQiOiIwNzAwZDk1MC02NTRmLTExZWItYmNjMC1jNWNhZTMxMTQ5MWEiLCJfbG9ja2FibGUiOnRydWUsInBpY3QiOiJpbWFnZXMvdXNlckxvZ28ucG5nIiwiZW1haWwiOiJ0ZXN0QGdhbmlzdGVyLmV1IiwiX21vZGlmaWVkT24iOjE2MzM2MTczNjg4NDQsIm5hbWUiOiJBZG1pbiBJc3RyYXRvciIsIl9sYWJlbFJlZiI6IkFkbWluIElzdHJhdG9yIn0.oRFUQ1gZ5oX5ZM8xcxpUoHLKcqczhT1Kk7OKQM8V1-k",
        "tokenExpiration": 7200000
    }
}
```

### Two Factor Auth
In case your user is set to use two-factor authentication you will receive an email with an 6 digit code. You need to send a second query with this code.
#### Query
![Login](./images/login2.png)

#### Result

The result is similar as the login without 2FA activated.

### Token Expiration

This token has an expiration time which can be configured in the .env file of your Ganister PLM server. By default, we set it to 2 hours. With our Ganister PLM client, we refresh the token if after half of the expiration delay a query is sent to the server. When using any other client, you need to take of this yourself. In our example we suppose we run our queries within 2 hours

### Set your token

From now on, you will have to pass the token for all your transactions.
In your headers, set the parameter **Authorization** with your token as the value.

## Data Creation

The data creation process can be altered depending on the PLM configuration you have implemented. To illustrate some of these diversity we will define that a Part as a mandatory field "name" whereas the Document does not have any mandatory field.

### Create a Part

As mentioned, the "name" property is a mandatory field in the Part nodetype. Therefore you have to supply this information.
Here is a first example where we don't provide the correct information.

#### Part creation attempt with a missing mandatory property
![Login](./images/failedPartCreation.png)

```json
{
  "properties": {
      "name": "Part 0001"
  }
}
```

#### Part creation success
![Login](./images/successPartCreation.png)
### Create a Document

In our example the document nodetype does not have any mandatory property. Therefore we can create a document instance with a post request with an empty body..

![Login](./images/successDoctCreation.png)

### Attach the Document to the Part
In order to attach a document to a part we use the following REST endpoint:
POST /nodes​/{nodetype}​/{nodeId}​/relationships​/{relationshipName}

- The identification of the target node has to be passed in the body
- Properties of the relationship can be passed, or must be passed if set as mandatory on the relationship definition.

```json
{
  "target": {
    "_type": "Document",
    "_id": "57e9ea30-2838-11ec-9ca2-bfd7a0eb2c6e"
  },
  "properties": {}
}
```

![Login](./images/docToPartAttachement.png)


## Data Fetching


### Get All Parts

GET /nodes​/{nodetype}

### Search for Documents

Here is an example to retrieve all the documents containing 'test' in their name.
POST /nodes​/{nodetype}​/search
```json 
{
    "maxResults": 50,
    "searchCriterias": {
        "properties.name": {
            "filterType": "text",
            "type": "contains",
            "filter": "test"
        }
    },
    "searchDate": 1634680799651,
    "searchType": "currentOn"
}
```
### Retrieve a Document
GET /nodes​/{nodetype}​/{nodeId}


### Get the Created Part

GET /nodes​/{nodetype}​/{nodeId}
### Get the Attached Document

GET /nodes​/{nodetype}​/{nodeId}/relationships​/{relationshipName}

## API Documentation

For more information about all our API routes, you can reach our Swagger from your installed instance.
https://<yourInstanceUrl>/api-docs