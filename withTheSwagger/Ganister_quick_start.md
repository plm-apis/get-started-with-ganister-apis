Ganister quick start

This is a short demo of Ganister capabilities

Ganister install
================

[Ganister - PLM Connecting the
dots](https://ganister.eu/useGanister/technicalApproach)

Navigation and create your first objects
========================================

Create a custom property
========================

To be updated

Use the APIs with the swagger
=============================

After starting Ganister on your local

Navigate to <http://localhost:8008/> and use this credentials

![](media/928b099df430aec6b889e402eae4a424.png)

On the start page click on

![](media/6317d258bbc251a5ee22547a5924bd79.png)

on the top right :

![](media/7a3953c52d71d0c38d84412c7051b0ef.png)

Then select

![](media/224563d92b20a752cadfa62542d70b68.png)

in the G-CONFIG page

![](media/d96f057e5e074ba54f21482160801bfa.png)

It opens the swagger:

![](media/7a2d15fe3cb007e6937da8b5d87ee72a.png)

For those who do not know what a Swagger is, read this XXXX

First all you need to signin to generate a token

Go to POST /users/signin and enter the following body

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

{
"email": "test\@ganister.eu",
"password": "ganister"
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

![](media/8d8c1af5cddf3719ac10099a1b8162e2.png)

Then press Execute to play the request. The swagger allow you to really request
the Ganister server.

You get a real resulst :

![](media/5b090151d29c805baa703bacb3bf9a42.png)

In the response, you will find your token :

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"token":
"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJHYW5pc3RlckFQSSIsInN1YiI6IjU2YTRhODIwLTM4NzYtMTFlYi1hOWY1LTlmZjU3MjVlOTc0NiIsImlhdCI6MTYwNzM1MDUwOTEwNCwiZXhwIjoxNjA3MzkzNzA5MTA0LCJlbWFpbCI6InRlc3RAZ2FuaXN0ZXIuZXUiLCJuYW1lIjoiQWRtaW4gSXN0cmF0b3IiLCJfaXNBZG1pbiI6dHJ1ZSwiYWN0aXZlIjp0cnVlfQ.72TaTH2istdZgbQEo4UxlYbKuHs6rZRhG8kwN1hEbhk"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Copy it and go to the top of the page and click on

![](media/ee1c397777ca835cc849303a4c07caaa.png)

![](media/ee1c397777ca835cc849303a4c07caaa.png)

It will open this popup in which you will paste the token value (the string like
this eyJhbGciOiJI…1hEbhk)

![](media/44db333956ac0cdd3500a0809a08ba73.png)

Then click on

![](media/44db333956ac0cdd3500a0809a08ba73.png)

And Close

![](media/dfc1bdb52a1dee3227ea7c83f93a113b.png)

![](media/dfc1bdb52a1dee3227ea7c83f93a113b.png)

You have now access to all the other request like this one allowing you to
search a part created from a certain date :

![](media/e2e26d628e0897c47adc7d8226513e0e.png)

By entering this filter in the body:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
{

  "maxResults": 0,

  "searchCriterias": {

    "_modifiedOn": {

      "dateFrom": 1607349600000,

      "dateTo": null,

      "filterType": "date",

      "type": "greaterThan"

    }

  },

  "searchType": "currentOn",

  "searchDate": "1607381999331"

}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Click on Execute and see the result

![](media/a301a97b0e6fa7a7eb78caf4f0136e9a.png)

We get our part even with custom property !

Use Postman with Ganister
=========================

To be updated
